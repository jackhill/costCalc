module Main where

import Test.Tasty
import qualified Test.Tasty.QuickCheck as QC

import Data.Decimal
import CostCalc

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [qcProps]

qcProps = testGroup "(QuickCheck)"
          [
          ]

