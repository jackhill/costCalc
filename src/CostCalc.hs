{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
module CostCalc (months, years, dollars, terabytes, tibibytes ,
                 monthlyCost, yearlyCost,
                 (|+|), (|/|), (|*|), (*|), (|*), (/|), zero) where

import Data.Metrology.Poly
import Text.Printf (printf, PrintfType)

-- Definition of Dimensions and Units for use of units package
-- Dimensions
data TimeDim = TimeDim
instance Dimension TimeDim

data MoneyDim = MoneyDim
instance Dimension MoneyDim

data StorageDim = StorageDim
instance Dimension StorageDim

type CostDim = MoneyDim :/ TimeDim

-- Units
data Month = Month
instance Unit Month where
  type BaseUnit Month = Canonical
  type DimOfUnit Month = TimeDim
instance Show Month where
  show _ = "m"

data Year = Year
instance Unit Year where
  type BaseUnit Year = Month
  conversionRatio _ = 12
instance Show Year where
  show _ = "y"

data Dollar = Dollar
instance Unit Dollar where
  type BaseUnit Dollar = Canonical
  type DimOfUnit Dollar = MoneyDim
instance Show Dollar where
  show _ = "$"

data Tibibyte = Tibibyte
instance Unit Tibibyte where
  type BaseUnit Tibibyte = Terabyte
  conversionRatio _ = (1000 / 1024 :: Rational)

data Terabyte = Terabyte
instance Unit Terabyte where
  type BaseUnit Terabyte = Canonical
  type DimOfUnit Terabyte = StorageDim

type MyLCSU = MkLCSU '[(TimeDim, Month), (MoneyDim, Dollar), (StorageDim, Terabyte)]

type Money = MkQu_DLN MoneyDim MyLCSU Rational
type Time  = MkQu_DLN TimeDim MyLCSU Rational
type Storage = MkQu_DLN StorageDim MyLCSU Rational

months :: Rational -> Time
months n = quOf n  Month

years :: Rational -> Time
years n = quOf n Year

dollars :: Rational -> Money
dollars n = quOf n Dollar

terabytes :: Rational -> Storage
terabytes n = quOf n Terabyte

tibibytes :: Rational -> Storage
tibibytes n = quOf n Tibibyte

monthlyCost :: (Unit (Lookup MoneyDim lcsu), Unit (Lookup TimeDim lcsu),
      Elem
        Dollar
        '[CanonicalUnit'
            (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu),
          CanonicalUnit'
            (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu)],
      Elem
        Month
        '[CanonicalUnit'
            (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu),
          CanonicalUnit'
            (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu)],
      Elem
        (CanonicalUnit'
           (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu))
        '[Dollar, Month],
      Elem
        (CanonicalUnit'
           (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu))
        '[Dollar, Month],
      PrintfType f) =>
     Qu [F MoneyDim One, F TimeDim (P Zero)] lcsu Rational -> f
monthlyCost c = printf "Cost per month %.2f" $ (fromRational $ c # (Dollar :/ Month) :: Double)


yearlyCost :: (Unit (Lookup MoneyDim lcsu), Unit (Lookup TimeDim lcsu),
      Elem
        Dollar
        '[CanonicalUnit'
            (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu),
          CanonicalUnit'
            (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu)],
      Elem
        Month
        '[CanonicalUnit'
            (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu),
          CanonicalUnit'
            (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu)],
      Elem
        (CanonicalUnit'
           (BaseUnit (Lookup MoneyDim lcsu)) (Lookup MoneyDim lcsu))
        '[Dollar, Month],
      Elem
        (CanonicalUnit'
           (BaseUnit (Lookup TimeDim lcsu)) (Lookup TimeDim lcsu))
        '[Dollar, Month],
      PrintfType f) =>
     Qu [F MoneyDim One, F TimeDim (P Zero)] lcsu Rational -> f
yearlyCost c = printf "Cost per year %.2f" $ (fromRational $ c # (Dollar :/ Year) :: Double)

